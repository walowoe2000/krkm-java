package edu.krkm.pz;

public class listConsole {

    public static void main( String[] args){
        listInter examp=new doubleLinkedListInter();
        System.out.println("Демонстрация добавления в список с выводом, на выходе должны получить 3 5 4  2");
        examp.pushStart(4);
        examp.pushBack(2);
        examp.pushStart(5);
        examp.pushMiddle(1,3);
        try {
            examp.pushMiddle(23, 4);
        }catch(Exception e){
            System.out.println("Споймали исключение на индекс");
        }
        examp.showAllItems();
        System.out.println("------------------------------------------------------------------------------");
        System.out.println("Демонстрация удаления со списка с выводом, на выходе должны получить 5 4 ");
        examp.deleteStart();
        examp.deleteBack();
        examp.showAllItems();
        System.out.println("------------------------------------------------------------------------------");
        System.out.println("Демонстрация замены в списке выводом, на выходе должны получить 3 4 ");
        examp.setDataStart(3);
        examp.showAllItems();
        System.out.println("------------------------------------------------------------------------------");
        System.out.println("Демонстрация работы конструктора списка с заданым размером");
        listInter examp2=new doubleLinkedListInter(4);
        examp2.showAllItems();
        //try catch демонстрация некорректного индекса
    };
}
