package edu.krkm.pz;

public interface listInter {
     int NULLVALUE=1000;
     void pushBack(int inData);
     void pushStart(int inData);
     void pushMiddle(int index, int inData);
     boolean deleteBack();
     boolean deleteStart();
     boolean deleteMiddle(int index);
     int showStart();
     int showBack();
     int showMiddle(int index);
     boolean setDataStart(int inData);
     boolean setDataBack(int inData);
     boolean setDataMiddle(int index, int inData);
     boolean showAllItems();
     boolean isEmpty();
     int getSize();
}
