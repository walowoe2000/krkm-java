package edu.krkm.pz;

public class nodeCore {
    int data;
    nodeCore next;
    nodeCore prev;

    public nodeCore(int inDate, nodeCore inPrev, nodeCore inNext){
        data=inDate;
        prev=inPrev;
        next=inNext;
    };
    public int getData(){
      return data;
    };
    public nodeCore getNext(){
      return next;
    };
    public nodeCore getPrev(){
        return prev;
    }
    public void setNext(nodeCore inNext){
        next=inNext;
    };
    public void setPrev(nodeCore inPrev){
        prev=inPrev;
    };
    public void setData(int inData){
        data=inData;
    }
}
