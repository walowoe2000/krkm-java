package edu.krkm.pz;


public class doubleLinkedListInter implements edu.krkm.pz.listInter {

    nodeCore head = null;
    int size=0;

    public doubleLinkedListInter() {
    }

    public doubleLinkedListInter(int inSize) {
        for (int i = 0; i < inSize; i++)
            pushBack(1);
    }

    public void pushBack(int inData) {
        if (isEmpty()) {
            pushStart(inData);
        } else {
            nodeCore current = head;
            while (current.getNext() != null) {
                current = current.getNext();
            }
            current.setNext(new nodeCore(inData, current, null));
            size++;
        }
    }

    public void pushStart(int inData) {
        if (isEmpty()) {
            head = new nodeCore(inData, null, null);
        } else {
            head.setPrev(new nodeCore(inData, null, head));
            head = head.getPrev();
        }
        size++;
    }

    @Override
    public int getSize() {
        return size;
    }

    public void pushMiddle(int index, int inData) {
        validator.validateIndex(index, this.size);
        if (isEmpty() || index == 1) {
            pushStart(inData);
        } else {
            nodeCore current = head;
            for (int i = 1; i <= index - 2; i++) {
                current = current.getNext();
            }
            nodeCore InNode = new nodeCore(inData, current, current.getNext());
            current.getNext().setPrev(InNode);
            current.setNext(InNode);
            size++;
        }

    }

    public boolean deleteBack() {
        if (head == null) {
            return false;
        } else {
            nodeCore current = head;
            while (current.getNext() != null) {
                current = current.getNext();
            }
            if (current.getPrev() == null) {
                head = null;
            } else {
                current.getPrev().setNext(null);
            }
            size--;
            return true;
        }
    }

    public boolean deleteStart() {
        if (head == null) {
            System.out.println("listInter is empty!");
            return false;
        } else {
            head = head.getNext();
            if (head != null) {
                head.setPrev(null);
            }
            size--;
            return true;
        }
    }

    public boolean deleteMiddle(int index) {
        validator.validateIndex(index, this.size);
        if (head == null) {
            return false;
        } else {
            if (index == 1) {
                deleteStart();
            } else {
                if (index == size + 1) {
                    deleteBack();
                } else {
                    nodeCore current = head;
                    for (int i = 1; i <= index - 2; i++) {
                        current = current.getNext();
                    }
                    current.setNext(current.getNext().getNext());
                    current.getNext().setPrev(current);
                    size--;
                }
            }
            return true;
        }
    }

    public int showStart() {
        if (head == null) {
            return NULLVALUE;
        } else {
            return head.getData();
        }
    }

    public int showBack() {
        if (head == null) {
            return NULLVALUE;
        } else {
            nodeCore current = head;
            while (current.getNext() != null) {
                if (current.getNext() != null)
                    current = current.getNext();
            }
            return current.getData();
        }
    }

    public int showMiddle(int index) {
        validator.validateIndex(index, this.size);
        if (head == null) {
            return NULLVALUE;
        } else {
            if (index == 1) {
                return showStart();
            } else {
                nodeCore current = head;
                for (int i = 1; i <= index - 2; i++) {
                    current = current.getNext();
                }
                return current.getNext().getData();
            }
        }
    }

    public boolean setDataStart(int inData) {
        if (isEmpty()) {
            return false;
        } else {
            head.setData(inData);
            return true;
        }
    }

    public boolean setDataBack(int inData) {
        if (isEmpty()) {
            return false;
        } else {
            nodeCore current = head;
            while (current.getNext() != null) {
                if (current.getNext() != null)
                    current = current.getNext();
            }
            current.setData(inData);
            return true;
        }
    }

    public boolean setDataMiddle(int index, int inData) {
        validator.validateIndex(index, this.size);
        if (isEmpty()) {
            return false;
        } else {
            if (index == 1) {
                setDataStart(inData);
            } else {
                nodeCore current = head;
                for (int i = 1; i <= index - 2; i++) {
                    current = current.getNext();
                }
                current.getNext().setData(inData);
            }
            return true;
        }
    }

    public boolean showAllItems() {
        if (isEmpty()) {
            return false;
        } else {
            nodeCore current = head;
            System.out.println("Item is: " + current.getData());
            while (current.getNext() != null) {
                current = current.getNext();
                System.out.println("Item is: " + current.getData());
            }
            return true;
        }
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }
}

