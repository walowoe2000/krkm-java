package edu.krkm.pz;

public class validator {
    public static void validateIndex(int index, int size){
        if (index > size || index < 1) {
            throw new IllegalArgumentException("Incorrect index");
        }
    }
}
