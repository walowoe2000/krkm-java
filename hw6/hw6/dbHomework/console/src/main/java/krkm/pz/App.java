package krkm.pz;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import java.sql.BatchUpdateException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws SQLException, ClassNotFoundException {

        companyCrud companyTable=new companyCrud();
        deviceCrud deviceCrud=new deviceCrud();
        List<company> testList=new ArrayList<>();
        testList.add(new company("Die Auslander","Germany",900));
        testList.add(new company("RusExp","Russia",11000));
        testList.add(new company("Rider","Poland",400));

        System.out.println("Start SELECT demonstration");
        companyTable.read().forEach((company x)-> System.out.println(
                "Company name: "+x.getName()+" country: "+x.getCountry()+" workers: "+x.getAmountOfWorkers()+"") );
        deviceCrud.read().forEach((device x)-> System.out.println(
                "Device name: "+x.getName()+" model: "+x.getModel()+" manufacturer: "+x.getManufacturer()+""
        ));
        System.out.println("------------------------------");
        System.out.println("Delete demonstration. Delete device with model oiP900");
        deviceCrud.delete("oiP900");
        deviceCrud.read().forEach((device x)-> System.out.println(
                "Device name: "+x.getName()+" model: "+x.getModel()+" manufacturer: "+x.getManufacturer()+""
        ));
        System.out.println("------------------------------");
        System.out.println("Update demonstration.");
        deviceCrud.update(new device("Desktop","you-2ok","Toshiba"));
        deviceCrud.read().forEach((device x)-> System.out.println(
                "Device name: "+x.getName()+" model: "+x.getModel()+" manufacturer: "+x.getManufacturer()+""
        ));
        System.out.println("------------------------------");
        System.out.println("Single add demonstration.");
        companyTable.add(new company("Panorama","Ukraine",1400) );
        try {
            companyTable.insert();
        }catch (MySQLIntegrityConstraintViolationException e){
            System.out.println("Dublicate pk");
        }
        companyTable.read().forEach((company x)-> System.out.println(
                "Company name: "+x.getName()+" country: "+x.getCountry()+" workers: "+x.getAmountOfWorkers()+"") );
        System.out.println("------------------------------");
        System.out.println("Multiple add demonstration.");
        companyTable.add(testList);
        companyTable.insert();
        companyTable.read().forEach((company x)-> System.out.println(
                "Company name: "+x.getName()+" country: "+x.getCountry()+" workers: "+x.getAmountOfWorkers()+"") );
    }
}
