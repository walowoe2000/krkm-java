CREATE TABLE IF NOT EXISTS company (
  amountOfWorkers int(10000) NOT NULL,
  name varchar(30) NOT NULL UNIQUE,
  country varchar(30) NOT NULL,
  PRIMARY KEY (name)
);

CREATE TABLE IF NOT EXISTS device (
  name varchar(30) NOT NULL,
  model varchar(30) NOT NULL,
  company varchar(30) NOT NULL,
  PRIMARY KEY (model),
  FOREIGN KEY(model) REFERENCES company(name) ON DELETE CASCADE ON UPDATE CASCADE
);