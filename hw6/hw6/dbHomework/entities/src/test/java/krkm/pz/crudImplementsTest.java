package krkm.pz;

import org.junit.*;
import org.junit.rules.ExpectedException;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Unit test for simple App.
 */
public class crudImplementsTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    testService CONNECTOR = new testService();
    companyCrud SINGLE_VALUE = new companyCrud(CONNECTOR);
    companyCrud MULTY_VALUE = new companyCrud(CONNECTOR);
    company FIRST_TEST_VALUE = new company("Apple", "USA", 2990);
    company SECOND_TEST_VALUE = new company("Samsung", "China", 1020);
    company UPDATE_VALUE = new company("Apple", "China", 10);

    public crudImplementsTest() throws SQLException {
    }

    @Before
    public void setUp() throws Exception {
        SINGLE_VALUE.add(FIRST_TEST_VALUE);
        List<company> listOfValues = new ArrayList<>();
        listOfValues.add(FIRST_TEST_VALUE);
        listOfValues.add(SECOND_TEST_VALUE);
        MULTY_VALUE.add(listOfValues);
    }

    @After
    public void tearDown() throws Exception {
        SINGLE_VALUE.clearTable();
        SINGLE_VALUE.clear();
        MULTY_VALUE.clear();
    }

    @Test
    public void read() throws SQLException, ClassNotFoundException {
        //GIVEN
        String expectedValue = FIRST_TEST_VALUE.getName();
        //WHEN
        SINGLE_VALUE.insert();
        List<company> resultList = SINGLE_VALUE.read();
        String resultValue = resultList.get(0).getName();
        //THEN
        Assert.assertEquals(expectedValue, resultValue);
    }

    @Test
    public void insertSingleValue() throws SQLException, ClassNotFoundException {
        //GIVEN
        int expectedSize = 1;
        //WHEN
        SINGLE_VALUE.insert();
        List<company> resultList = SINGLE_VALUE.read();
        int resultSize = resultList.size();
        //THEN
        Assert.assertEquals(expectedSize, resultSize);
    }

    @Test
    public void insertMultyValues() throws SQLException, ClassNotFoundException {
        //GIVEN
        int expectedSize = 2;
        //WHEN
        MULTY_VALUE.insert();
        List<company> resultList = MULTY_VALUE.read();
        int resultSize = resultList.size();
        //THEN
        Assert.assertEquals(expectedSize, resultSize);
    }

    @Test
    public void updateValue() throws SQLException, ClassNotFoundException {
        //GIVEN
        String expectedValue = UPDATE_VALUE.getCountry();
        //WHEN
        SINGLE_VALUE.insert();
        SINGLE_VALUE.update(UPDATE_VALUE);
        String resultValue = SINGLE_VALUE.read().get(0).getCountry();
        //THEN
        Assert.assertEquals(expectedValue, resultValue);
    }

    @Test
    public void deleteValue() throws SQLException, ClassNotFoundException {
        //GIVEN
        int expectedSize = 1;
        //WHEN
        MULTY_VALUE.insert();
        MULTY_VALUE.delete(FIRST_TEST_VALUE.name);
        int resultSize = MULTY_VALUE.read().size();
        Assert.assertEquals(expectedSize, resultSize);
    }
}
