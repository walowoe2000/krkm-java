package krkm.pz;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Properties;

public class testService extends service {
    String path = "/home/linux/GitPrj/homework/hw6/dbHomework/entities/src/test/db.properties";
    public testService() throws SQLException {
            Properties properties = new Properties();
            try (
                    InputStream input = new FileInputStream(path)) {
                try {
                    properties.load(input);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                JDBC_DRIVER = properties.getProperty("driver");
                if (JDBC_DRIVER != null) {
                    System.setProperty("driver", JDBC_DRIVER);
                }
                DATABASE_URL = properties.getProperty("url");
                USER = properties.getProperty("user");
                PASSWORD = properties.getProperty("password");
            } catch (
                    IOException e) {
                e.printStackTrace();
            }
            connectionPool = mySimplePool.create(DATABASE_URL,USER,PASSWORD);
        }
    }
