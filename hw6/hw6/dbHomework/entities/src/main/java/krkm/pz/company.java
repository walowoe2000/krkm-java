package krkm.pz;

public class company {
    String name;
    String country;
    Integer amountOfWorkers;

    public company(String newName, String newCountry, Integer newAmount) {
        name = newName;
        country = newCountry;
        amountOfWorkers = newAmount;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setAmountOfWorkers(Integer mountOfWorkers) {
        this.amountOfWorkers = mountOfWorkers;
    }

    public Integer getAmountOfWorkers() {
        return amountOfWorkers;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }
}
