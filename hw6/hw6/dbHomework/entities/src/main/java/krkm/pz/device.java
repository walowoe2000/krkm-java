package krkm.pz;

import java.math.BigInteger;

public class device {
    String name;
    String model;
    String manufacturer;

    public device(String newName, String newModel, String newManufacturer){
        name=newName;
        model=newModel;
        manufacturer=newManufacturer;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getModel() {
        return model;
    }
}
