package krkm.pz;

import javax.naming.ldap.PagedResultsControl;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class deviceCrud extends crud_abstract<device> {


    protected deviceCrud() throws SQLException {
    }

    @Override
    public void insert_OneEntitie(device newData) throws SQLException, ClassNotFoundException {
        PreparedStatement preparedStatement = dbConnector.getPreparedStatemnt("INSERT INTO device (name, model, manufacturer) VALUES (?,?,?)");
        preparedStatement.setString(1,newData.getName());
        preparedStatement.setString(2,newData.getModel());
        preparedStatement.setString(3,newData.getManufacturer());
        preparedStatement.executeUpdate();
        preparedStatement.close();
        dbConnector.closeConnect();
    }

    @Override
    public void insert_MultyEntities(List<device> newData) throws SQLException {
        try{
            PreparedStatement preparedStatement = dbConnector.getPreparedStatemnt("INSERT INTO device VALUES (?,?,?");
            dbConnector.connection.setAutoCommit(false);
            newData.forEach((device x)-> {
                try {
                    preparedStatement.setString(1,x.getName());
                    preparedStatement.setString(2,x.getModel());
                    preparedStatement.setString(3,x.getManufacturer());
                    preparedStatement.addBatch();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
            int[] result = preparedStatement.executeBatch();
            System.out.println("The number of rows inserted: "+ result.length);
            dbConnector.connection.commit();
            preparedStatement.close();
        }catch(Exception e){
            e.printStackTrace();
            dbConnector.connection.rollback();
        } finally{
            dbConnector.connection.close();
        }
    }

    @Override
    public boolean update(device newData) throws SQLException, ClassNotFoundException {
        PreparedStatement preparedStatement = dbConnector.getPreparedStatemnt("UPDATE device SET name = ?, model = ?, manufacturer = ? WHERE model = ?");
        preparedStatement.setString(1,newData.getName());
        preparedStatement.setString(2,newData.getModel());
        preparedStatement.setString(3,newData.getManufacturer());
        preparedStatement.setString(4,newData.getModel());
        preparedStatement.executeUpdate();
        preparedStatement.close();
        dbConnector.closeConnect();
        return true;
    }

    @Override
    public void delete(String id) throws SQLException, ClassNotFoundException {
        PreparedStatement preparedStatement = dbConnector.getPreparedStatemnt("DELETE FROM device WHERE model = ?");
        preparedStatement.setString(1,id);
        preparedStatement.executeUpdate();
        preparedStatement.close();
        dbConnector.closeConnect();
    }

    @Override
    public List<device> read() throws SQLException, ClassNotFoundException {
        PreparedStatement preparedStatement = dbConnector.getPreparedStatemnt("SELECT * FROM device");
        ResultSet resultSet = preparedStatement.executeQuery();
        List<device> returnList = new ArrayList<>();
        while (resultSet.next()) {
            returnList.add(new device(resultSet.getString("name"),resultSet.getString("model"),resultSet.getString("manufacturer")));
        }
        preparedStatement.close();
        dbConnector.closeConnect();
        return returnList;
    }
    @Override
    public void clearTable() throws SQLException, ClassNotFoundException {
        PreparedStatement preparedStatement = dbConnector.getPreparedStatemnt("DELETE  FROM device");
        preparedStatement.executeUpdate();
        preparedStatement.close();
        dbConnector.closeConnect();

    }
}
