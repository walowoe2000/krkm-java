package krkm.pz;

import java.sql.BatchUpdateException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class companyCrud extends crud_abstract<company>{

    protected companyCrud() throws SQLException {
    }
    protected  companyCrud(service newConnector) throws SQLException {
        super();
        dbConnector=newConnector;
    }
    @Override
    public void insert_OneEntitie(company newData) throws SQLException, ClassNotFoundException {
        PreparedStatement preparedStatement = dbConnector.getPreparedStatemnt("INSERT INTO company (name,country,amountOfWorkers) VALUES (?,?,?)");
        preparedStatement.setString(1,newData.getName());
        preparedStatement.setString(2,newData.getCountry());
        preparedStatement.setInt(3,newData.getAmountOfWorkers());
        preparedStatement.executeUpdate();
        preparedStatement.close();
        dbConnector.closeConnect();

    }

    @Override
    public void insert_MultyEntities(List<company> newData) throws SQLException {
        try{
            PreparedStatement preparedStatement = dbConnector.getPreparedStatemnt("INSERT INTO company (name,country,amountOfWorkers) VALUES (?,?,?)");
            dbConnector.connection.setAutoCommit(false);
            newData.forEach((company x)-> {
                try {
                    preparedStatement.setString(1,x.getName());
                    preparedStatement.setString(2,x.getCountry());
                    preparedStatement.setInt(3,x.getAmountOfWorkers());
                    preparedStatement.addBatch();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
            int[] result = preparedStatement.executeBatch();
            System.out.println("The number of rows inserted: "+ result.length);
            dbConnector.connection.commit();
            preparedStatement.close();
        }catch(SQLException | ClassNotFoundException e){
            System.out.println("Some problems, rollback");
            dbConnector.connection.rollback();
        }finally{
            dbConnector.connection.close();
        }
    }

    @Override
    public boolean update(company newData) throws SQLException, ClassNotFoundException {
        PreparedStatement preparedStatement = dbConnector.getPreparedStatemnt("UPDATE company SET name = ?, country = ?, amountOfWorkers = ? WHERE name = ?");
        preparedStatement.setString(1,newData.getName());
        preparedStatement.setString(2,newData.getCountry());
        preparedStatement.setInt(3,newData.getAmountOfWorkers());
        preparedStatement.setString(4,newData.getName());
        preparedStatement.executeUpdate();
        preparedStatement.close();
        dbConnector.closeConnect();
        return true;
    }
    @Override
    public void delete(String id) throws SQLException, ClassNotFoundException {
        PreparedStatement preparedStatement = dbConnector.getPreparedStatemnt("DELETE FROM company WHERE name = ?");
        preparedStatement.setString(1,id);
        preparedStatement.executeUpdate();
        preparedStatement.close();
        dbConnector.closeConnect();
    }

    @Override
    public List<company> read() throws SQLException, ClassNotFoundException {
        PreparedStatement preparedStatement = dbConnector.getPreparedStatemnt("SELECT * FROM company");
        ResultSet resultSet = preparedStatement.executeQuery();
        List<company> returnList = new ArrayList<>();
        while (resultSet.next()) {
            returnList.add(new company(resultSet.getString("name"),resultSet.getString("country"),resultSet.getInt("amountOfWorkers")));
        }
        preparedStatement.close();
        dbConnector.closeConnect();
        return returnList;
    }
    public void clear(){
        listOfValues.clear();
    }

    @Override
    public void clearTable() throws SQLException, ClassNotFoundException {
        PreparedStatement preparedStatement = dbConnector.getPreparedStatemnt("DELETE FROM company");
        preparedStatement.executeUpdate();
        preparedStatement.close();
        dbConnector.closeConnect();

    }
}
