package krkm.pz;
import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;

import javax.xml.transform.Result;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.List;
import java.util.Properties;

public class service{
    String pathConf = "/home/linux/GitPrj/homework/hw6/dbHomework/entities/src/main/resources/db.properties";
    String DATABASE_URL;
    String JDBC_DRIVER;
    String USER;
    String PASSWORD;
    Connection connection = null;
    Statement statement = null;
    connectionPoolInterface connectionPool;

    public service() throws SQLException {
        Properties properties = new Properties();
        try (
                InputStream input = new FileInputStream(pathConf)) {
            try {
                properties.load(input);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            JDBC_DRIVER = properties.getProperty("driver");
            if (JDBC_DRIVER != null) {
                System.setProperty("driver", JDBC_DRIVER);
            }
            DATABASE_URL = properties.getProperty("url");
            USER = properties.getProperty("user");
            PASSWORD = properties.getProperty("password");
        } catch (
                IOException e) {
            e.printStackTrace();
        }
        connectionPool = mySimplePool.create(DATABASE_URL,USER,PASSWORD);
    }

    public PreparedStatement getPreparedStatemnt(String SQL) throws SQLException, ClassNotFoundException {
        setConnection();
        return connection.prepareStatement(SQL);
    }

    public void setConnection() throws ClassNotFoundException, SQLException {
        connection = connectionPool.getConnection();
    }

    public void closeConnect() throws ClassNotFoundException, SQLException {
        if (connection != null)
            connection.close();
    }
}
