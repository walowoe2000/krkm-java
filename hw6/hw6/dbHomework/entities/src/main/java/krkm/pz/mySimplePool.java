package krkm.pz;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class mySimplePool implements connectionPoolInterface {
    private String url;
    private String user;
    private String password;
    private List<Connection> connectionPool;
    private List<Connection> usedConnections = new ArrayList<>();
    private static int INITIAL__POOL__SIZE = 10;

    public mySimplePool(String newUrl, String newUser, String newPassword){
        url=newUrl;
        user=newUser;
        password=newPassword;
    }
    public mySimplePool(String newUrl, String newUser, String newPassword, List<Connection> newPool) {
        url=newUrl;
        user=newUser;
        password=newPassword;
        connectionPool=newPool;
    }

    public static mySimplePool create(
            String url, String user,
            String password) throws SQLException {

        List<Connection> pool = new ArrayList<>(INITIAL__POOL__SIZE);
        for (int i = 0; i < INITIAL__POOL__SIZE; i++) {
            pool.add(createConnection(url, user, password));
        }
        return new mySimplePool(url, user, password, pool);
    }

    @Override
    public Connection getConnection() {
        Connection connection = connectionPool
                .remove(connectionPool.size() - 1);
        usedConnections.add(connection);
        return connection;
    }

    @Override
    public boolean releaseConnection(Connection setConnect) {
        connectionPool.add(setConnect);
        return usedConnections.remove(setConnect);
    }

    @Override
    public String getUrl() {
        return null;
    }

    @Override
    public String getUser() {
        return null;
    }

    @Override
    public String getPassword() {
        return null;
    }
    private static Connection createConnection(
            String url, String user, String password)
            throws SQLException {
        return DriverManager.getConnection(url, user, password);
    }

    public int getSize() {
        return connectionPool.size() + usedConnections.size();
    }

    //standard getters
}
