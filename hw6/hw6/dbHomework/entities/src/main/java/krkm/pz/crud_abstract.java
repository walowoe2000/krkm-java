package krkm.pz;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public abstract class crud_abstract<T> implements crud<T> {
    protected List<T> listOfValues = new ArrayList<>();
    protected service dbConnector;

    protected crud_abstract() throws SQLException {
        dbConnector= new service();
    }
    protected crud_abstract(service newConnector){
        dbConnector=newConnector;
    }
    @Override
    public void insert() throws SQLException, ClassNotFoundException {
        if (!listOfValues.isEmpty()) {
            if (listOfValues.size() == 1) {
                insert_OneEntitie(listOfValues.get(0));
            } else {
                insert_MultyEntities(listOfValues);
            }
            listOfValues.clear();
        }
    }

    public List<T> read() throws SQLException, ClassNotFoundException {
        return null;
    }

    public void add(T newValue) {
        listOfValues.add(newValue);
    }

    public void add(List<T> newValues) {
        listOfValues.addAll(newValues);
    }

    public void clearTable() throws SQLException, ClassNotFoundException {};

}
