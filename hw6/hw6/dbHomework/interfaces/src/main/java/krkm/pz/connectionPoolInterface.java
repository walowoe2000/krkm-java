package krkm.pz;

import java.sql.Connection;

public interface connectionPoolInterface {
    Connection getConnection();
    boolean releaseConnection(Connection setConnect);
    String getUrl();
    String getUser();
    String getPassword();
}
