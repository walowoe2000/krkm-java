package krkm.pz;

import java.sql.SQLException;
import java.util.List;

public interface crud<T> {
    //INSERT
    void insert_OneEntitie(T newData) throws SQLException, ClassNotFoundException;
    void insert_MultyEntities(List<T> newData) throws SQLException;
    void insert() throws SQLException, ClassNotFoundException;
    //UPDATE
    boolean update(T newData) throws SQLException, ClassNotFoundException;
    //DELETE
    void delete(String id) throws SQLException, ClassNotFoundException;
    //SELECT
    List<T> read() throws SQLException, ClassNotFoundException;
}
