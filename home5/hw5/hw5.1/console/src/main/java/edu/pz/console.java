package edu.pz;

import java.util.HashMap;
import java.util.Map;

public class console {

    public static void main(String[] args) {
        authorsListener subObj=new authorsListener();
        listOfBooks obj=new listOfBooks(subObj);
        book book1 = new book("Мастер и Маргарита","Булгаков","12345");
        book book3 = new book("Собачье сердце","Булгаков","33214");
        book book2 = new book("Маугли","Киплинг","132102");
        obj.addBook(book1);
        obj.addBook(book2);
        obj.addBook(book3);
        Map<String,Integer> mapa = subObj.getAuthors();
        mapa.forEach((key,value)->System.out.println("Автор: "+key+" количество книг : "+value));
        obj.removeBook(book1);
        mapa = subObj.getAuthors();
        mapa.forEach((key,value)->System.out.println("Автор: "+key+" количество книг : "+value));
    }
}
