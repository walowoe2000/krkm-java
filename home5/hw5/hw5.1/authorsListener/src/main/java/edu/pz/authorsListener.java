package edu.pz;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class authorsListener implements listener {
    private Map<String,Integer> books = new HashMap<String, Integer>();
    @Override
    public void add(String author) {
        if(books.get(author)!=null)
            books.put(author,books.get(author)+1);
        else
        books.put(author,1);
    }

    @Override
    public void remove(String author) {
        if(books.get(author)!=null) {
            if(books.get(author)==1){
                books.remove(author);
            }else{
                books.put(author,books.get(author)-1);
            }
        }

    }
    public Map<String,Integer> getAuthors(){
        return books;
    }
}
