package edu.pz;

public class book {
    private String title;
    private String author;
    private String isbn;

    public book (String bookTitle, String bookAuthor, String bookIsbn){
        title=bookTitle;
        author=bookAuthor;
        isbn=bookIsbn;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getIsbn() {
        return isbn;
    }
}
