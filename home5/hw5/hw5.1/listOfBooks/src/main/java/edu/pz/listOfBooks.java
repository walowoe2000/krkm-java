package edu.pz;

import java.util.ArrayList;

public class listOfBooks {
    private ArrayList<book> books = new ArrayList<book>();
    private authorsListener listener;
    public listOfBooks(authorsListener newListener){
        listener=newListener;
    }
    public void addBook(book newBook){
        books.add(newBook);
        listener.add(newBook.getAuthor());
    }
    public void removeBook(book oldBook){
        books.remove(oldBook);
        listener.remove(oldBook.getAuthor());
    }
}
