package org.example;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Hello world!
 *
 */
public class App 
{
    public static <T> Stream<T> zip(Stream<T> first, Stream<T> second){



        ArrayList<T> firstList = (ArrayList<T>) first.collect(Collectors.toList());
        ArrayList<T> secondList= (ArrayList<T>) second.collect(Collectors.toList());
        ArrayList<T> finalList= new ArrayList<>();
        int min = firstList.size()<secondList.size()?firstList.size():secondList.size();
        int i=0;
            while(i<min){
                finalList.add(firstList.get(i));
                finalList.add(secondList.get(i));
                i++;
            }
            return (Stream <T>) Stream.of(finalList);
    };
    public static void main( String[] args )
    {
        Integer[] mas1 = {1,3,5,7,9};
        Integer[] mas2 = {2,4};
        Stream <Integer> fisrtStream= Arrays.stream(mas1);
        Stream <Integer> secondStream= Arrays.stream(mas2);
        Stream <Integer> finalStream =zip(fisrtStream,secondStream);
        Object[] arrFinal = finalStream.toArray();
        Arrays.stream(arrFinal).forEach((Object x)->System.out.println(x));

    };
}
